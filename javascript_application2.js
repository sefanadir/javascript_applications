// Initilize integer variable
var harezmi=0;
// calculate funcion adds to numbers and return it
function calculate(number) {
  return number +3;
}

// assing return value to number_var
var number_var=calculate(harezmi);
console.log("number is equal to",number_var);

// check_number function checks whether number is even or odd
function check_number(number) {
  if(number%2==0){
    return "This number is even";
  }
  else{
    return "This number is odd";
  }
}

// Display whether result is even or odd
console.log(check_number(harezmi));

// Anonim function
var result=function check_number(number) {

  if(number%2==0){
    return "This number is even";
  }
  else{
    return "This number is odd";
  }
}

console.log(result(harezmi));
