// ***************** ForEach Function ****************** //
// The forEach function must take a function as a parameter.
function display(array_index){
  console.log(array_index);
}
array.forEach(display);
// ***************************************************** //

// **************** Own forEach function *************** //
array=[1,2,3,4];

function for_each(fonksiyon,array){
  for (var i = 0; i < array.length; i++) {
    fonksiyon(array[i]);
  }
}

function display(variable){
  console.log(variable);
}

for_each(display,array);
