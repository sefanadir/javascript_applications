// Initialize null to data
var data=null;

// This function retrieves data from the database
function get_data_from_database(callback){
  // wait for data retrieval
  console.log("getting data...");

  // setTimeout function waits 1000 milliseconds
  setTimeout(function(){
    data="afedersiniz_front_end";
    // callback function=display_result function
    callback();
  },1000)

}
// This function display data to screen
function display_result(){
  console.log("received data: ",data);
}
// Call function for data retrieval
get_data_from_database(display_result);
