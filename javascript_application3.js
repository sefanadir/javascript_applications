// Get a number from user with prompt() function
var number_var=prompt();

// convert getting number that is string to integer number
number_var=parseInt(number_var);

// this function decreases number and displays numbers at each step
function big_to_small(number){

  for(var i=number; i>=0;i=i-1){
    console.log(i);
  }
}
// call big_to_small to order
big_to_small(number_var);

// define a list and initialize
var array=[7,5,6,2,4];

// display elements of list with for loop
for (var i = 0; i < array.length; i++) {
  console.log(array[i]);
}
