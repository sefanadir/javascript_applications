// ***** Filter Function Example ***** //
var list=[4,2,6,5,8,1,4];

// Filter function returns boolean variable
var odd_number=list.filter(function (number) {
  return number%2==1;
});

console.log(odd_number);
