// DATA TYPES //
var number=2;       // integer variable
var decimal = 2.2;  // decimal variable
var text="lyk";     // string  variable
var checking=true;  // boolean variable

// list variable
var liste=[
  2,
  "ahmet",
  true,
  2.1,
  ["onur","aziz",2,true]
]

// object variable
var object_dictionary={

  lyk: "linux yaz kampı",
  meaning_of_life:42,
  visible:true,
  numbers: [ 0,1,2,3,4,5 ],
  user: { name: "ali", age: 21}
};

// Initialize variable
var number_variable=2;

// If number is bigger than 3
if (number_variable>3) {
  console.log("This number is bigger than 3");
}
// If number is not bigger than 3
else {
  console.log("This number is not bigger than 3");
}

// for loop
for (var i = 0; i < 10; i++) {
  console.log(i);
}

// Initialize integer variable for while loop
var count=0;
// while loop
while(count<5){
  console.log("count=",count);
  // increase count variable
  ++count;
}

// switch-case condition
switch (count) {
  case 0:
    console.log("count is equal to",count);
    break;
  case 1:
    console.log("count is equal to",count);
    break;
  default:
      console.log("count is not found");
}
