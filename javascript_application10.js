var basket=[
  {
    name:"shoes",
    cost:40
  },
  {
    name:"honey",
    cost:30
  },
  {
    name:"keyboard",
    cost:100
  }
];

var cost_list=basket.map(function (number) {
  return number.cost;
});
console.log(cost_list);

var total_cost=cost_list.reduce(function(total,element){
  return parseInt(total)+parseInt(element);
},0);

console.log(total_cost);
