// *********** List Example ********** //
var list=[4,5,2,4,9,5,2];

// Add new element to list with push function
list.push(7);
console.log(list);

// pop function removes end element of list
list.pop();
console.log(list);

// shift function removes first element of list
list.shift();
console.log(list);

// unshift function adds first element to list
list.unshift(8);
console.log(list);

// indexOf function return index of element of list
var index=list.indexOf(2);
console.log(index);

// Add new element to list with push function
list.push(7);
console.log(list);

// pop function removes end element of list
list.pop();
console.log(list);

// shift function removes first element of list
list.shift();
console.log(list);

// unshift function adds first element to list
list.unshift(8);
console.log(list);

// indexOf function return index of element of list
var index=list.indexOf(2);
console.log(index);

// returns until 5. index
list.splice(5)
console.log(list);

// splice function deletes the given index range
list.splice(4,6);
console.log(list);

// splice function return value is deleting index
var splice_list=list.splice(4,6);
console.log(splice_list);

// print end element of list
console.log(list[list.length-1]);

// from 1. index to 3. index
// return 1. and 2. index
var slice_list=list.slice(1,3);
console.log(slice_list);
